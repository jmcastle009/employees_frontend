# employees_frontend

this repository contains frontend of a project to manage employees

## Requirements

- Node 16 or higher


## Installation

To clone this repository, is necessary use the following commands:

```
cd existing_dir
git clone https://gitlab.com/jmcastle009/employees_frontend.git
```
Then access to the directory created for the clone command.
To download all modules necessaries,  use the following commands 

```
npm install
npm start
```

The port by default is **3000**

Before start this application, remenmber start the backend project stored in
 [Employees_Frontend](https://gitlab.com/jmcastle009/employess_backend)

## Tools and Technologies
 - ReactJS
 - TypeScript
 - Moment
 - Bootswatch
 - SweetAlert2